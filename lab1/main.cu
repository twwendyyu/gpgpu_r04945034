#include <cstdio>
#include <cstdint>
#include <cstdlib>
#include "../utils/SyncedMemory.h"
#include "lab1.h"
using namespace std;

#define CHECK {\
	auto e = cudaDeviceSynchronize();\
	if (e != cudaSuccess) {\
		printf("At " __FILE__ ":%d, %s\n", __LINE__, cudaGetErrorString(e));\
		abort();\
	}\
}

int main(int argc, char **argv)
{
	Lab1VideoGenerator g;
	Lab1VideoInfo i;

	g.get_info(i);
	if (i.w == 0 or i.h == 0 or i.n_frame == 0 or i.fps_n == 0 or i.fps_d == 0) {
		puts("Cannot be zero");
		abort();
	} else if (i.w%2 != 0 or i.h%2 != 0) {
		puts("Only even frame size is supported");
		abort();
	}
	unsigned FRAME_SIZE = i.w*i.h*3/2;
	MemoryBuffer<uint8_t> frameb(FRAME_SIZE); 		/* invoke an object 'frameb' with type 'unit8_t' using class 'MemoryBuffer' */ 		/* Alloc(); (not C standard) allocate memory on cpu */
	auto frames = frameb.CreateSync(FRAME_SIZE);	/* auto invoke an object 'frames' using class 'SyncedMemory'? *//* Reset();  reset pointer of cpu_, gpu_ and size */
	FILE *fp = fopen("test.y4m", "wb");
	
	fprintf(fp, "YUV4MPEG2 W%d H%d F%d:%d Ip A1:1 C420\n", i.w, i.h, i.fps_n, i.fps_d);

	for (unsigned j = 0; j < i.n_frame; ++j) {		/* generating frames */
		fputs("FRAME\n", fp);
		g.Generate(frames.get_gpu_wo());			/* get_gpu_wo(); change state */ /* Generate(*gpu_=*yuv) */
		fwrite(frames.get_cpu_ro(), sizeof(uint8_t), FRAME_SIZE, fp);
	}

	fclose(fp);
	return 0;
}
