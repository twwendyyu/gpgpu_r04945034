#include "lab1.h"
#include <stdlib.h>
#include <stdio.h>


static const unsigned W = 640;
static const unsigned H = 480;
static const unsigned NFRAME = 240;
static const int BLOCK_SIZE = 320;
const int blockCount = (W*H/4+BLOCK_SIZE-1)/BLOCK_SIZE;

struct Lab1ChannelInfo {
	unsigned start;
	unsigned color;
	unsigned size;	
};

struct Lab1VideoGenerator::Impl {
	int t = 0;
};

Lab1VideoGenerator::Lab1VideoGenerator(): impl(new Impl) {
}

Lab1VideoGenerator::~Lab1VideoGenerator() {}

void Lab1VideoGenerator::get_info(Lab1VideoInfo &info) {
	info.w = W;
	info.h = H;
	info.n_frame = NFRAME;
	// fps = 24/1 = 24
	info.fps_n = 24;
	info.fps_d = 1;
};
void initialize1DArray(unsigned *data, unsigned size)
{
	for (unsigned i = 0; i < size; ++i)
		data[i] = 0.0;
}

/*
** RGB color data (colorblind safe)
*/
unsigned samplingRGBData(unsigned color, unsigned lb, unsigned ub){
	unsigned result;
		result = lb+(ub-lb)*color/255;
	return result;
}
/*
** Convert RGB to YUV
*/
void rgb2yuv(unsigned *color, unsigned color_r, unsigned color_g, unsigned color_b){
	
	color[0] =  0.299*color_r + 0.587*color_g + 0.114*color_b;
	color[1] = -0.169*color_r - 0.331*color_g + 0.500*color_b + 128;
	color[2] =  0.500*color_r - 0.419*color_g - 0.081*color_b + 128;
}

/*
** cudaMemset each pixel
*/
void cudaMemsetPixel(uint8_t *yuv, unsigned i, unsigned j, unsigned color_y, unsigned color_u, unsigned color_v){
	
	unsigned start_y = 2*i*W + (2*j); /* coz both horizontal and vertical resolution is halved. (4:2:0)*/
	unsigned start_uv = i*W/2 +j;
	
	cudaMemset(yuv+start_y, color_y,2);  				/* Channel Y-up */
	cudaMemset(yuv+start_y+W, color_y,2);  				/* Channel Y-lo */
	cudaMemset((yuv+W*H)+start_uv, color_u,1);  		/* Channel U */
	cudaMemset((yuv+W*H+W*H/4)+start_uv, color_v,1);  	/* Channel V */
}
/*
** Generate julia set
*/
__global__ void juliaKernel(unsigned *value, unsigned h, unsigned w, float dim, float offset, float stop, float c1, float c2, unsigned k0){
	unsigned size = h*w;	// h = H/2; w = W/2; 
	unsigned idx = blockIdx.x*blockDim.x + threadIdx.x;
	
	if (idx < size){
		unsigned k = 0;
		float i, j, x, y, temp;		 
		
		i = (float)(idx/w);
		j = (float)(idx - i*w);
		
		x = (__fdividef(dim*i,h)-offset);//*(ctheta+stheta);
		y = (__fdividef(dim*j,w)-offset);//*(ctheta-stheta);		
		
		
		while (x*x + y*y < stop && k < 255){
			temp = x*x - y*y + c1;
			y = 2*x*y + c2;
			x = temp;
			k = k+k0;		
		}
		value[idx] = k;
	}
}
/* 
** cudaMemset(void* devPtr, int value, size_t count) 
** devPtr - Pointer to device memory
** value - 	Value to set for each byte of specified memory
** count - 	Size in bytes to set
*/
void Lab1VideoGenerator::Generate(uint8_t *yuv) {
	
	/* Background */
	//struct Lab1ChannelInfo y = {0, (impl->t)*255/NFRAME, W*H};
	struct Lab1ChannelInfo y = {0, 0, W*H};
	struct Lab1ChannelInfo u = {y.size, 128, W*H/4};
	struct Lab1ChannelInfo v = {y.size+u.size, 128, W*H/4};

	cudaMemset(yuv+y.start, y.color, y.size);  /* Channel Y */	
	cudaMemset(yuv+u.start, u.color, u.size);  /* Channel U */
	cudaMemset(yuv+v.start, v.color, v.size);  /* Channel V */
	
	/* Draw (each pixel) */
	//unsigned value;
	//unsigned *color = new unsigned[3];
	unsigned h = H/2;
	unsigned w = W/2;
	unsigned color_size = h*w;
	unsigned *color = new unsigned[color_size];
		initialize1DArray(color, color_size);
	unsigned *color2 = new unsigned[color_size];
		initialize1DArray(color2, color_size);
	unsigned *colorNew = new unsigned[3];
		initialize1DArray(colorNew, 3);
	unsigned *gcolor, *gcolor2;
	
	float minScale, dim, offset, stop, c1, c2;
	unsigned k0;
	/* call outer juliaKernel */
	minScale = 0.004;
	dim = 4.0 * (minScale-1)/NFRAME + 1;
	offset = dim/2;
	stop = dim;	
	k0 = 5;
	if ((impl->t) < NFRAME/2){
			c1 = 0.0 - 0.22*log((float)(impl->t))/log(10);
			c2 = 0.68+0.000416*((float)(impl->t));
	}else{
			c1 = 0.0 - 0.22*log((float)(NFRAME/2-1))/log(10) + 0.25*log((float)((impl->t) - NFRAME/2))/log(10);
			c2 = 0.68+0.000416*(NFRAME/2-1)-0.000416*((impl->t) - NFRAME/2);
	}
	cudaMalloc((void **)&gcolor, sizeof(unsigned)*color_size);	
	juliaKernel<<<blockCount, BLOCK_SIZE>>> (gcolor, h, w, dim, offset, stop, c1, c2, k0);	
	cudaMemcpy(color, gcolor, sizeof(unsigned)*color_size, cudaMemcpyDeviceToHost);
	
	/* call internal juliaKernel */
	minScale = 0.004;
	dim = 4.0 * (minScale-1)/NFRAME + 1;
	offset = dim/2;
	stop = dim;
	k0 = 5;
	if ((impl->t)>120 && (impl->t)<174){
		c1 = 0.0 - 0.22*log((float)((impl->t)-95))/log(10);
		c2 = 0.68+0.000416*((float)((impl->t)-95));

		cudaMalloc((void **)&gcolor2, sizeof(unsigned)*color_size);	
		juliaKernel<<<blockCount, BLOCK_SIZE>>> (gcolor2, h, w, dim, offset, stop, c1, c2, k0);	
		cudaMemcpy(color2, gcolor2, sizeof(unsigned)*color_size, cudaMemcpyDeviceToHost);
	}
	
	
	/* cudaMemsetPixel */
	for (unsigned i = 0; i < h; ++i){ // coz 4:2:0, H -> H/2, W -> W/2
		for (unsigned j = 0; j < w; ++j){
			unsigned id = i*w+j;
			
			/* outer julia */
			if (color[id] > 250){				
				rgb2yuv(colorNew, 248, 249, samplingRGBData(color[id],214,232));
			}else if(color[id] < 251 && color[id] > 240){
				rgb2yuv(colorNew, samplingRGBData(color[id],192,248), samplingRGBData(color[id],234,249), samplingRGBData(color[id],214,232));
			}else if(color[id] < 15){
				rgb2yuv(colorNew, samplingRGBData(color[id],57,120), samplingRGBData(color[id],63,152), samplingRGBData(color[id],73,152)); 
			}else{			
				rgb2yuv(colorNew, samplingRGBData(color[id],47,192), samplingRGBData(color[id],61,234), samplingRGBData(color[id],89,214));
			}
			
			/* internal julia*/
			if (((impl->t)>120 && (impl->t)<174)){
				if(color[id] == 255){					
					
					if(color2[id] > 150){
						rgb2yuv(colorNew, samplingRGBData(255-color2[id],47,82), samplingRGBData(255-color2[id],61,128), samplingRGBData(255-color2[id],73,196));
					}else if(color2[id] < 15){
						rgb2yuv(colorNew, samplingRGBData(255-color2[id],192,248), samplingRGBData(255-color2[id],234,249), samplingRGBData(255-color2[id],214,232)); 
					}else{			
						rgb2yuv(colorNew, samplingRGBData(255-color2[id],47,192), samplingRGBData(255-color2[id],61,234), samplingRGBData(255-color2[id],89,214));
					}
				}
			}/* end of internal julia */				
			
			cudaMemsetPixel(yuv, i, j, colorNew[0], colorNew[1], colorNew[2]);					
			
		}/* end j*/
	}/* end i*/		
	
	
	/* free */
	delete[] color;
	delete[] color2;
	delete[] colorNew;
	cudaFree(gcolor);
	cudaFree(gcolor2);
	
	/* increase frame index */
	++(impl->t);
}
