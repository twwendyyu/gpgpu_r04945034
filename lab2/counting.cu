#include "counting.h"
#include <cstdio>
#include <cassert>
#include <thrust/scan.h>
#include <thrust/transform.h>
#include <thrust/functional.h>
#include <thrust/device_ptr.h>
#include <thrust/execution_policy.h>

#define BLOCK_SIZE  500 //must be 500
#define MAX_SIZE 20000000


__device__ __host__ int CeilDiv(int a, int b) { return (a-1)/b + 1; }
__device__ __host__ int CeilAlign(int a, int b) { return CeilDiv(a, b) * b; }

struct char2int_op : public thrust::unary_function<char,int>
{
	__host__ __device__ int operator()(char x)
	{
        	if (x == '\n')
			return 0;
                else
			return 1;
        }
};

void CountPosition1(const char *text, int *pos, int text_size)
{	
	char2int_op op;
		 
	thrust::transform(thrust::device, text, text + text_size, pos, op);
	thrust::inclusive_scan_by_key(thrust::device, pos, pos+text_size, pos, pos);
}


__global__ void char2int_Kernel (int size, const char *text, int *pos)
{
	int idx = blockIdx.x*blockDim.x+threadIdx.x;
	if (idx < size)
	{
		if (text[idx] == '\n')
			pos[idx] = 0;
		else
			pos[idx] = 1;
	}
}

__global__ void getWordInfo_Kernel (int size, int batch_id, int block_num, int *pos, int *gLength, int *gSaved, int *gLastId)
{	
	int i, id_ori, id_now, val_ori, val_now, count, bound;
	int idx = blockIdx.x*blockDim.x+threadIdx.x;

    if (idx < size)
    {
		if (threadIdx.x == 0)
		{		
			int temp = batch_id*MAX_SIZE;
			
			id_ori = idx;
			val_ori = pos[temp + id_ori];
			gSaved[id_ori] = 1;
			count = 0;
			
			if ( blockIdx.x == block_num -1)
				bound = size - idx;
			else
				bound = blockDim.x;
			
			
			for (i = 0; i < bound; ++i)
			{		
				id_now = idx + i;
				val_now = pos[temp + id_now];
				
				if (val_ori == val_now)
				{
					gSaved[id_now] = 0; 
					count ++; 
				}
				else //save current length
				{	
					gSaved[id_ori] = 1;
					gLength[id_ori] = count;

					id_ori = id_now;
					val_ori = pos[temp + id_ori]; 
					count = 1;					
				}
			}
			// if it is the last char in this block, save length too
			gSaved[id_ori] = 1;
			gLength[id_ori] = count;
						
			gLastId[blockIdx.x] = id_ori;
		}
	}
}

__global__ void deRep_Kernel (int size, int batch_id, int *pos, int *gLength, int *gSaved, int *gLastId)
{	
	int idx = blockIdx.x*blockDim.x+threadIdx.x;
	
    if (idx < size-1) // ignore last one
    {
		int id_data = gLastId[idx];
		int temp = batch_id*MAX_SIZE;
		int id_rep =  idx*BLOCK_SIZE + (BLOCK_SIZE-1);	
		
		if (pos[temp+id_rep] == pos[temp+id_rep+1])
		{
			gLength[id_data] += gLength[id_rep+1];
			gSaved[id_rep+1] = 0;
			gLength[id_rep+1] = 0;
		}
	}
}

__global__ void writePos_Kernel (int size, int batch_id, int *pos, int *gLength, int *gSaved)
{
	int idx = blockIdx.x*blockDim.x+threadIdx.x;
	int i;

    if (idx < size)
    {	
		int temp = batch_id*MAX_SIZE;
		
		if (gSaved[idx]==1)
		{				
			for (i = 0; i < gLength[idx]; ++i)
			{
				if (pos[temp+ idx+i]==1)
					pos[temp+ idx+i] = i+1;
			}
		}	
	}
}

__global__ void gatherInfo_Kernel (int size, int batch_id, int block_num, int *gLength, int *gLastId, int *gGridLength)
{
	int idx = blockIdx.x*blockDim.x+threadIdx.x;		

    if (idx < size)
	{	
		int temp = 2*(batch_id+idx);
		int id_data = gLastId[block_num-1];
		
		gGridLength[temp] = gLength[MAX_SIZE*idx];
		gGridLength[temp+1] = gLength[id_data];
	}
	
}

__global__ void deRep_rewrite_Kernel (int size, int *pos, int *gGridLength)
{
	int idx = blockIdx.x*blockDim.x+threadIdx.x;		

    if (idx < size -1) // ignore last one
	{
		int i;
		int id_len = 2*idx +1; // start from fist batch's last length: gGridLength[1]
		int id_data = MAX_SIZE*idx + (MAX_SIZE-1);
		
		if (pos[id_data] > 0 && pos[id_data+1] > 0)
		{
			for (i = 0; i < gGridLength[id_len+1]; ++i)
				pos[id_data+1 +i] = pos[id_data+1 +i] + gGridLength[id_len];
		}
	}
}

void CountPosition2(const char *text, int *pos, int text_size)
{	
	int batch_size, blockCount;
	int MAX_BLOCK_SIZE = MAX_SIZE/BLOCK_SIZE;
	int batch_num = (text_size + MAX_SIZE-1)/MAX_SIZE;
	int batch_id = 0;	
	
	int *gLength, *gSaved, *gLastId, *gGridLength;	

	cudaMalloc((void **)&gLength, sizeof(int)*MAX_SIZE);
 	cudaMalloc((void **)&gSaved, sizeof(int)*MAX_SIZE);
	cudaMalloc((void **)&gLastId, sizeof(int)*MAX_BLOCK_SIZE);
	cudaMalloc((void **)&gGridLength, sizeof(int)*batch_num*2);
	
	cudaMemset(gGridLength,0,sizeof(int)*batch_num*2);
	cudaMemset(gLength,0,sizeof(int)*MAX_SIZE);
	cudaMemset(gSaved,0,sizeof(int)*MAX_SIZE);	
	cudaMemset(gLastId,0,sizeof(int)*MAX_BLOCK_SIZE);
	
	//printf("char2int\n"); 	
	char2int_Kernel <<<((text_size +1024-1)/1024), 1024>>> (text_size, text, pos);	
		
	while (batch_id < batch_num)
	{	
		if (batch_id == batch_num -1)
			batch_size = text_size - (batch_num-1)*MAX_SIZE;
		else
			batch_size = MAX_SIZE;
		
		blockCount = (batch_size +BLOCK_SIZE-1)/BLOCK_SIZE;
			
		//printf("batch_id\t%d\nbatch_size\t%d\n",batch_id,batch_size);					
		//printf("getWrodInfo\n");
		getWordInfo_Kernel <<<blockCount, BLOCK_SIZE>>> (batch_size, batch_id, blockCount, pos, gLength, gSaved, gLastId);
		if (blockCount > 1)
		{
			//printf("deRep\n"); 		
			deRep_Kernel <<<((blockCount +1024-1)/1024),1024>>> (blockCount, batch_id, pos, gLength, gSaved, gLastId);
		}
		//printf("writePos\n"); 	
		writePos_Kernel <<<blockCount, BLOCK_SIZE>>> (batch_size, batch_id, pos, gLength, gSaved);

		if (batch_num >1)
		{
			//printf("gatherInfo\n");
			gatherInfo_Kernel <<< 1,1 >>> (1, batch_id, blockCount, gLength, gLastId, gGridLength);
		}
		
/*//debug ////////////////////////////////////////////////////////////////////////////////////////////////////		
		int len_return[MAX_SIZE], saved_return[MAX_SIZE], lastId_return[MAX_BLOCK_SIZE];
		char fname2[100];
		sprintf(fname2,"len_%d.txt",MAX_SIZE);
		FILE *fptr2 = fopen(fname2,"w");
		
		memset(len_return,0,MAX_SIZE);
		memset(saved_return,0,MAX_SIZE);
		memset(lastId_return,0,MAX_BLOCK_SIZE);
		
		cudaMemcpy(len_return, gLength, sizeof(int)*MAX_SIZE, cudaMemcpyDeviceToHost);
		cudaMemcpy(saved_return, gSaved, sizeof(int)*MAX_SIZE, cudaMemcpyDeviceToHost);
		cudaMemcpy(lastId_return, gLastId, sizeof(int)*MAX_BLOCK_SIZE, cudaMemcpyDeviceToHost);		
		
		printf("\nlastId\t");
		for(int i = 0; i < blockCount; ++i){
                printf("%d ",lastId_return[i]);
                if((i+1)%50==0) printf("\n");
        }
        printf("\nlen\n");
		for(int i = 0; i < batch_size; ++i){
				fprintf(fptr2,"%d ",len_return[i]);
                //printf("%d ",len_return[i]);
                if((i+1)%50==0)
				{
					fprintf(fptr2,"\n");
					//printf("\n");
				}
        }
        printf("\nsaved\n");
		for(int i = 0; i < batch_size; ++i){
                printf("%d ",saved_return[i]);
                if((i+1)%50==0) printf("\n");
        }
        printf("\n");
		fclose(fptr2);
//debug ////////////////////////////////////////////////////////////////////////////////////////////////////*/
		
		batch_id++;
	}
	if (batch_num >1)
	{
		//printf("deRep_rewrite_Kernel\n");
		deRep_rewrite_Kernel <<<((batch_num +512-1)/512),512>>> (batch_num, pos, gGridLength);//between each batch
	}
	
/*//debug ////////////////////////////////////////////////////////////////////////////////////////////////////
	char text_return[text_size];
	int pos_return[text_size], gridlen_return[batch_num*2];
	char fname[100];
	sprintf(fname,"pos_%d.txt",MAX_SIZE);
	FILE *fptr = fopen(fname,"w");
	
	memset(text_return,'\0',text_size);
	memset(pos_return,0,text_size);
	memset(gridlen_return,0,batch_num*2);

	cudaMemcpy(text_return, text, sizeof(char)*text_size, cudaMemcpyDeviceToHost);
	cudaMemcpy(pos_return, pos, sizeof(int)*text_size, cudaMemcpyDeviceToHost);
	cudaMemcpy(gridlen_return, gGridLength, sizeof(int)*batch_num*2, cudaMemcpyDeviceToHost);
	
	printf("\ngridlen\n");
	for(int i = 0; i < batch_num*2; ++i){
    	printf("%d ",gridlen_return[i]);
        if((i+1)%50==0) printf("\n");
    }
	printf("\ntext\n");
	for(int i = 0; i < text_size; ++i){
		if (text_return[i]=='\n')
			printf("  ");
		else
			printf("%c ",text_return[i]);
		if((i+1)%50==0) printf("\n");
	}
	printf("\npos print to file: %s\n",fname);
	for(int i = 0; i < text_size; ++i){
		
		//printf("%d ",pos_return[i]);
		fprintf(fptr, "%d ",pos_return[i]);
		
		if((i+1)%50==0)
		{
			//printf("\n");
			fprintf(fptr,"\n");
		}
	}
	fclose(fptr);
//debug ////////////////////////////////////////////////////////////////////////////////////////////////////*/

	//clear	
	cudaFree(gLength);
	cudaFree(gSaved);
	cudaFree(gLastId);
	cudaFree(gGridLength);

}
