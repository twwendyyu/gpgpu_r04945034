#include "lab3.h"
#include <cstdio>

__device__ __host__ int CeilDiv(int a, int b) { return (a-1)/b + 1; }
__device__ __host__ int CeilAlign(int a, int b) { return CeilDiv(a, b) * b; }

__global__ void SimpleClone(
	const float *background,
	const float *target,
	const float *mask,
	float *output,
	const int wb, const int hb, const int wt, const int ht,
	const int oy, const int ox
)
{
	const int yt = blockIdx.y * blockDim.y + threadIdx.y;
	const int xt = blockIdx.x * blockDim.x + threadIdx.x;
	const int curt = wt*yt+xt;
	if (yt < ht && xt < wt && mask[curt] > 127.0f) {
		const int yb = oy+yt, xb = ox+xt;
		const int curb = wb*yb+xb;
		if (0 <= yb && yb < hb && 0 <= xb && xb < wb) {
			output[curb*3+0] = target[curt*3+0];
			output[curb*3+1] = target[curt*3+1];
			output[curb*3+2] = target[curt*3+2];
		}
	}
}
__device__ void AddEachChannel(float *base, int id_base, const float *adder, int id_adder, int c, int n)
{
	for (int i = 0; i < n; ++i)
		base[id_base*n+i] += c*adder[id_adder*n+i];
}

__global__ void CalculateFixed(
	const float *background,
	const float *target,
	const float *mask,
	float *fixed,
	const int wb, const int hb, const int wt, const int ht,
	const int oy, const int ox
)
{
	const int yt = blockIdx.y * blockDim.y + threadIdx.y;
	const int xt = blockIdx.x * blockDim.x + threadIdx.x;
	const int curt = wt*yt+xt;
	
	if (yt < ht && xt < wt) 
	{
		const int yb = oy+yt, xb = ox+xt;
		const int curb = wb*yb+xb;
		
		// Initialize
		fixed[curt*3+0] = 0.0f;
		fixed[curt*3+1] = 0.0f;
		fixed[curt*3+2] = 0.0f;
		
		// West, East, North and South Pixels
		// West
		if (yt == 0 || mask[curt-wt] < 127.0f)
			AddEachChannel(fixed, curt, background, curb-wb, 1, 3);
			
		if (xt > 0)
		{
			AddEachChannel(fixed, curt, target, curt, 1, 3);
			AddEachChannel(fixed, curt, target, curt-1, -1, 3);
		}
		
		// East
		if (yt == ht-1 || mask[curt+wt] < 127.0f)
			AddEachChannel(fixed, curt, background, curb+wb, 1, 3);
			
		if (xt < wt-1)
		{
			AddEachChannel(fixed, curt, target, curt, 1, 3);
			AddEachChannel(fixed, curt, target, curt+1, -1, 3);
		}
		
		// North
		if (xt == 0 || mask[curt-1] < 127.0f)
			AddEachChannel(fixed, curt, background, curb-1, 1, 3);
			
		if (yt > 0)
		{
			AddEachChannel(fixed, curt, target, curt, 1, 3);
			AddEachChannel(fixed, curt, target, curt-wt, -1, 3);
		}
		
		// South
		if (xt == wt-1 || mask[curt+1] < 127.0f)
			AddEachChannel(fixed, curt, background, curb+1, 1, 3);
			
		if (yt < ht-1)
		{
			AddEachChannel(fixed, curt, target, curt, 1, 3);
			AddEachChannel(fixed, curt, target, curt+wt, -1, 3);
		}
	}	
}

__global__ void PoissonImageCloningIteration(
	float *fixed,
	const float *mask,
	float *in,
	float *out,
	const int wt, const int ht	
)
{
	const int yt = blockIdx.y * blockDim.y + threadIdx.y;
	const int xt = blockIdx.x * blockDim.x + threadIdx.x;
	const int curt = wt*yt+xt;
	
	if (yt < ht && xt < wt)
	{
		// Initialize
		out[curt*3+0] = 0.0f;
		out[curt*3+1] = 0.0f;
		out[curt*3+2] = 0.0f;
		
		// Copy from fixed[] to out[]
		AddEachChannel(out, curt, fixed, curt, 1, 3);
		
		// West, East, North and South Pixels
		if (xt > 0 && mask[curt-1] > 127.0f)
			AddEachChannel(out, curt, in, curt-1, 1, 3);

		if (xt < wt-1 && mask[curt+1] > 127.0f)
			AddEachChannel(out, curt, in, curt+1, 1, 3);

		if (yt > 0 && mask[curt-wt] > 127.0f)
			AddEachChannel(out, curt, in, curt-wt, 1, 3);

		if (yt < ht-1 && mask[curt+wt] > 127.0f)
			AddEachChannel(out, curt, in, curt+wt, 1, 3);
		
		// Times Back
		out[curt*3+0] *= 0.25;
		out[curt*3+1] *= 0.25;
		out[curt*3+2] *= 0.25;
	}	
}


void PoissonImageCloning(
	const float *background,
	const float *target,
	const float *mask,
	float *output,
	const int wb, const int hb, const int wt, const int ht,
	const int oy, const int ox
)
{
	// set up
	float *fixed, *buf1, *buf2;
	cudaMalloc(&fixed, 3 * wt*ht * sizeof(float));
	cudaMalloc(&buf1, 3 * wt*ht * sizeof(float));
	cudaMalloc(&buf2, 3 * wt*ht * sizeof(float));

	// initialize the iteraction
	dim3 gdim(CeilDiv(wt, 32), CeilDiv(ht, 16)), bdim(32, 16);
	CalculateFixed << <gdim, bdim >> > (
		background, target, mask, fixed,
		wb, hb, wt, ht, oy, ox);
	cudaMemcpy(buf1, target, sizeof(float) * 3 * wt*ht, cudaMemcpyDeviceToDevice);

	// iterate
	for (int i = 0; i < 10000; i++) {
		PoissonImageCloningIteration << <gdim, bdim >> > (
			fixed, mask, buf1, buf2, wt, ht
			);
		PoissonImageCloningIteration << <gdim, bdim >> > (
			fixed, mask, buf2, buf1, wt, ht
			);
	}

	// copy the image back
	cudaMemcpy(output, background, wb*hb*sizeof(float)*3, cudaMemcpyDeviceToDevice);
	SimpleClone<<<dim3(CeilDiv(wt,32), CeilDiv(ht,16)), dim3(32,16)>>>(
		background, buf1, mask, output,
		wb, hb, wt, ht, oy, ox
	);
}